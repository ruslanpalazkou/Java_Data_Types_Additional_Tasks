package src.additional;

import java.util.Scanner;

public class Task6 {
    // Calculate the circumference and area of a circle of the same given radius R.
    public static void main(String[] args) {
        float radius;
        float area;
        float circumference;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the Radius: ");
        radius = scanner.nextFloat();

        area = (float)(3.14 * radius * radius);
        circumference = (float)(2 * 3.14 * radius);

        System.out.println("Area is: " + area);
        System.out.println("Circumference is: " + circumference);
    }
}