package src.additional;

import java.util.Scanner;

public class Task8 {
    // Find the product of the digits of a given four-digit number.
    static int getProduct(String str) {
        int product = 1;
        for (int i = 0; i < str.length(); i++) {
            product *= str.charAt(i) - '0';
        }
        return product;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        String string = Integer.toString(number);

        if (string.length() < 4) {
            System.out.println("Error: number must be four digits");
            return;
        }
        System.out.println("Digits numbers is: " + getProduct(string));
    }
}