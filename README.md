# Java Data Types Additional Tasks

### Description
Task1: Given two real numbers x and y. Calculate their sum, difference, product and quotient.
Task6: Calculate the circumference and area of a circle of the same given radius R.
Task8: Find the product of the digits of a given four-digit number.